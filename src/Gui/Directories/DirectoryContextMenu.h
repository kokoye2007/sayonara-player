/* DirectoryContextMenu.h */

/* Copyright (C) 2011-2020  Lucio Carreras
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DIRECTORYCONTEXTMENU_H
#define DIRECTORYCONTEXTMENU_H

#include "Gui/Utils/ContextMenu/LibraryContextMenu.h"

/**
 * @brief The DirectoryContextMenu class
 * @ingroup GuiDirectories
 */
class DirectoryContextMenu :
		public Library::ContextMenu
{
	Q_OBJECT
	PIMPL(DirectoryContextMenu)

signals:
	void sig_create_dir_clicked();
	void sig_rename_clicked();
	void sig_rename_by_tag_clicked();
	void sig_collapse_all_clicked();
	void sig_move_to_lib(LibraryId id);
	void sig_copy_to_lib(LibraryId id);

public:
	enum Mode
	{
		Dir=0,
		File
	};

	enum Entry
	{
		EntryCreateDir = Library::ContextMenu::EntryLast,
		EntryRename = Library::ContextMenu::EntryLast << 1,
		EntryRenameByTag = Library::ContextMenu::EntryLast << 2,
		EntryCollapseAll = Library::ContextMenu::EntryLast << 3,
		EntryMoveToLib = Library::ContextMenu::EntryLast << 4,
		EntryCopyToLib = Library::ContextMenu::EntryLast << 5
	};

	DirectoryContextMenu(Mode mode, QWidget* parent);
	~DirectoryContextMenu() override;

	void refresh(int count=0);

	ContextMenu::Entries get_entries() const override;
	void show_actions(ContextMenu::Entries entries) override;
	void show_directory_action(DirectoryContextMenu::Entry entry, bool b);

private slots:
	void library_move_action_triggered();
	void library_copy_action_triggered();

protected:
	void language_changed() override;
	void skin_changed() override;
};

#endif // DIRECTORYCONTEXTMENU_H
